package hotciv.all;

import hotciv.common.GameImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;
import hotciv.variants.factories.AlphaCivFactory;
import org.junit.*;

import static org.junit.Assert.*;

public class TestObservers {

    GameImpl game;
    ObserverDummy observerDummy;

    @Before
    public void setup(){
        try {
            game = new GameImpl(new AlphaCivFactory());
        } catch (IllegalPositionException e) {
            e.printStackTrace();
        }
        observerDummy = new ObserverDummy(game);
    }

    @Test
    public void shouldInstantiateAndRemoveObserver(){
        boolean isRemoved = game.removeObserver(observerDummy);
        assertTrue("Should return true if the unit is added and then removed", isRemoved);
    }

    @Test
    public void shouldBeAbleToCallWorldChangedAt() throws IllegalPositionException{
        boolean methodCalled = observerDummy.hasChangedWorld;
        assertFalse("Should check the method variable is false", methodCalled);
        observerDummy.worldChangedAt(new Position(1,1));
        methodCalled = observerDummy.hasChangedWorld;
        assertTrue("The methods is called", methodCalled);
    }

    @Test
    public void shouldBeAbleToCallTurnEnds() throws IllegalPositionException{
        boolean methodCalled = observerDummy.hasEndedTurn;
        assertFalse("Should check the method variable is false", methodCalled);
        observerDummy.turnEnds(Player.BLUE, 0);
        methodCalled = observerDummy.hasEndedTurn;
        assertTrue("The methods is called", methodCalled);
    }

    @Test
    public void shouldBeAbleToCallTileFocusChangedAt() throws IllegalPositionException {
        boolean methodCalled = observerDummy.hasChangedTileFocus;
        assertFalse("Should check the method variable is false", methodCalled);
        observerDummy.tileFocusChangedAt(new Position(1,1));
        methodCalled = observerDummy.hasChangedTileFocus;
        assertTrue("The methods is called", methodCalled);
    }

    public class ObserverDummy implements GameObserver{

        public boolean hasChangedWorld = false;
        public boolean hasEndedTurn = false;
        public boolean hasChangedTileFocus = false;

        public ObserverDummy(Game game) {
            game.addObserver(this);
        }

        public void worldChangedAt(Position pos) {
            hasChangedWorld = changeBool(hasChangedWorld);
        }


        public void turnEnds(Player nextPlayer, int age) {
            hasEndedTurn = changeBool(hasEndedTurn);
        }

        public void tileFocusChangedAt(Position position) {
            hasChangedTileFocus = changeBool(hasChangedTileFocus);
        }

        public boolean changeBool(boolean boolToChange){
            boolToChange = !boolToChange;
            return boolToChange;
        }
    }

}
