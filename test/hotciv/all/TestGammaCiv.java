package hotciv.all;

import hotciv.common.*;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;
import hotciv.variants.factories.GammaCivFactory;

import org.junit.*;
import static org.junit.Assert.*;

import java.security.KeyException;

public class TestGammaCiv {

    private GameImpl game;
    private Unit unit;
    private City city;
    private Player player;

    @Before
    public void setup() throws IllegalPositionException, KeyException {

        game = new GameImpl(new GammaCivFactory());

        //Setup of the different units
        game.addUnit(new Position(2,2), new UnitImpl(Player.RED, GameConstants.SETTLER));
        game.addUnit(new Position(6,6), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        game.addUnit(new Position(8,8), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
    }

    @Test
    public void shouldPerformRedSettlerAction() throws IllegalPositionException{

       unit = game.getUnitAt(new Position(2,2));
       assertNotNull("Unit at position (2,2) should be in the map", unit);
       game.performUnitActionAt(new Position(2,2));
       Object objNull = game.getUnitAt(new Position(2,2));
       assertNull("Unit at position (2,2) should be deleted", objNull);
       city = game.getCityAt(new Position(2,2));
       assertNotNull("There should be a red city at position (2,2)", city);
       player = game.getCityAt(new Position(2,2)).getOwner();
       assertEquals("Owner of city at position (2,2) should be red", Player.RED, player);
    }

    @Test
    public void shouldPerformBlueSettlerAction() throws IllegalPositionException{

        game.endOfTurn();
        unit = game.getUnitAt(new Position(6,6));
        assertNotNull("Unit at position (6,6) should be in the map", unit);
        game.performUnitActionAt(new Position(6,6));
        Object objNull = game.getUnitAt(new Position(6,6));
        assertNull("Unit at position (6,6) should be deleted", objNull);
        city = game.getCityAt(new Position(6,6));
        assertNotNull("There should be a red city at position (6,6)", city);
        player = game.getCityAt(new Position(6,6)).getOwner();
        assertEquals("Owner of city at position (6,6) should be red", Player.BLUE, player);
    }


    @Test
    public void shouldPerformBlueArcherAction() throws IllegalPositionException{

        game.endOfTurn();
        game.performUnitActionAt(new Position(8,8));
        int defense = game.getUnitAt(new Position(8,8)).getDefensiveStrength();
        assertEquals("Should double blue archers defensive strength", 6, defense);

    }

    @Test
    public void shouldInvokeFortifyOnBlueArcher() throws IllegalPositionException{

        game.endOfTurn();
        // Should invoke archer fortify
        game.performUnitActionAt(new Position(8,8));
        game.performUnitActionAt(new Position(8,8));
        int defensiveStrength = game.getUnitAt(new Position(8, 8)).getDefensiveStrength();
        assertEquals("Should invoke fortify", 3, defensiveStrength);
    }

    @Test
    public void shouldNotBeAbleToMoveFortifyingUnit() throws IllegalPositionException {

        game.performUnitActionAt(new Position(8,8));
        game.moveUnit(new Position(8,8), new Position(9,9));
        unit = game.getUnitAt(new Position(8,8));
        assertNotNull("The unit should still be at position (8,8)", unit);
    }
}
