package hotciv.all;

import hotciv.common.*;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

import java.security.KeyException;

import hotciv.variants.factories.AlphaCivFactory;

import org.junit.*;
import static org.junit.Assert.*;

public class TestAlphaCiv {
    private GameImpl game;
    private Player player;
    private City city;
    private Tile tile;
    private Unit unit;
    private Boolean moveBool;

    @Before
    public void setUp() throws IllegalPositionException, KeyException {

        game = new GameImpl(new AlphaCivFactory());
    }

    /* TESTS FOR CITIES */

    @Test
    public void shouldHaveRedCityAt1_1() throws IllegalPositionException {

        city = game.getCityAt(new Position(1,1));
        assertNotNull("There should be a city at (1,1)", city);
        player = city.getOwner();
        assertEquals("City at (1,1) should be owned by red", Player.RED, player);
    }

    @Test
    public void shouldHaveBlueCityAt4_1() throws IllegalPositionException {

        city = game.getCityAt(new Position(4,1));
        assertNotNull("There should be a city at (4,1)", city);
        player = city.getOwner();
        assertEquals("City at (4,1) should be owned by blue", Player.BLUE, player);
    }

    @Test
    public void shouldHaveAPopulationOf1() throws IllegalPositionException {

        city = game.getCityAt(new Position(4,1));
        int size = city.getSize();
        assertEquals("The population of the city should be 1",1 ,size);
    }

    @Test
    public void shouldHaveAProductionOf6After1Round() throws IllegalPositionException {

        CityImpl city = (CityImpl) game.getCityAt(new Position(4,1));
        HelperMethods.endOfNRounds(game, 1);
        int production = city.getProductionSize();
        assertEquals("After one round the city's production should be 6", 6, production);
    }

    @Test
    public void shouldHaveAProductionOf18After3Round() throws IllegalPositionException {

        CityImpl city = (CityImpl) game.getCityAt(new Position(4,1));
        HelperMethods.endOfNRounds(game, 3);
        int production = city.getProductionSize();
        assertEquals("After three round the city's production should be 8", 8, production);
    }

    @Test
    public void shouldSetProductionTypeForCityToArcher() throws IllegalPositionException{

        game.changeProductionInCityAt(new Position(1,1), GameConstants.ARCHER);
        String unitType = game.getCityAt(new Position(1,1)).getProduction();
        assertEquals("The production type for city at position (1,1) should be archer", GameConstants.ARCHER, unitType);
    }

    /* TEST FOR TILES */

    @Test
    public void shouldHaveOceanAt1_0() throws IllegalPositionException {

        tile = game.getTileAt(new Position(1, 0));
        assertNotNull("There should be a tile at (1,0)", tile);
        String s = tile.getTypeString();
        assertEquals("Tile should be a tile of type ocean", GameConstants.OCEANS, s);
    }

    @Test
    public void shouldHaveMountainAt2_2() throws IllegalPositionException {

        tile = game.getTileAt(new Position(2, 2));
        assertNotNull("There should be a tile at (2,2)", tile);
        String s = tile.getTypeString();
        assertEquals("Tile should be tile of type mountain", GameConstants.MOUNTAINS, s);
    }

    @Test
    public void shouldHaveHillsAt0_1() throws IllegalPositionException {

        tile = game.getTileAt(new Position(0, 1));
        assertNotNull("There should be a tile at (0,1)", tile);
        String s = tile.getTypeString();
        assertEquals("Tile should be tile of type hills", GameConstants.HILLS, s);
    }

    @Test
    public void shouldHavePlainsAt4_5() throws IllegalPositionException {

        tile = game.getTileAt(new Position(4,5));
        assertNotNull("There should be a tile at (4,5)", tile);
        String s = tile.getTypeString();
        assertEquals("There should be a tile of type plains", GameConstants.PLAINS, s);
    }

    /* TESTS FOR YEARS, TURN AND ROUND */

    @Test
    public void shouldReturnRedAsFirstPlayerInTurn() {

        player = game.getPlayerInTurn();
        assertEquals("Should return RED as the first player", player.RED, player);
    }

    @Test
    public void shouldReturnBlueAsSecondPlayerInTurn() {

        game.endOfTurn(); // Increase round by 1
        player = game.getPlayerInTurn();
        assertEquals("Should return Blue as the second player", player.BLUE, player);
    }

    @Test
    public void shouldReturnYear4000BC() {

        int y = game.getAge();
        assertEquals("Should print 4000 BC", -4000, y);
    }

    @Test
    public void shouldReturnYear3000BCAfter10Rounds() {

        HelperMethods.endOfNRounds(game,10);
        int y = game.getAge();
        assertEquals("Should return year 3000 BC after 10 rounds", -3000, y);
    }

    @Test
    public void shouldReturnRedAsWinnerAt3000BC(){

        HelperMethods.endOfNRounds(game, 10);
        int age = game.getAge();
        assertEquals("The age should be 3000 BC", -3000, age);
        player = game.getWinner();
        assertEquals("The winner should be red at 3000 BC", Player.RED, player);
    }


    @Test
    public void shouldReturnNoWinnerBeforeAndAfter3000BC(){

        HelperMethods.endOfNRounds(game, 1);
        player = game.getWinner();
        assertEquals("There should be no winner before 3000 BC", null, player);
        HelperMethods.endOfNRounds(game, 14);
        player = game.getWinner();
        assertEquals("There should be no winner after 3000 BC", null, player);
    }

    /* TESTS OF UNITS */

    @Test
    public void shouldHaveARedUnitAt2_0() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(2,0));
        assertNotNull("Should have a unit at position (2,0)", unit);
        player = unit.getOwner();
        assertEquals("Unit at (2,0) should have a red owner", Player.RED, player);
    }

    @Test
    public void shouldHaveAnArcherAt2_0() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(2,0));
        String type = unit.getTypeString();
        assertEquals("Unit at (2,0) should have an type of archer", GameConstants.ARCHER, type);
    }

    @Test
    public void shouldHaveABlueUnitAt3_2() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(3,2));
        assertNotNull("Should have a unit at position (3,2)", unit);
        player = unit.getOwner();
        assertEquals("Unit at (3,2) should have a blue owner", Player.BLUE, player);
    }

    @Test
    public void shouldHaveAnLegionAt3_2() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(3,2));
        String type = unit.getTypeString();
        assertEquals("Unit at (3,2) should have an type of legion", GameConstants.LEGION, type);
    }

    @Test
    public void shouldHaveARedUnitAt4_3() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(4,3));
        assertNotNull("Should have a unit at position (4,3)", unit);
        player = unit.getOwner();
        assertEquals("Unit at (4,3) should have a red owner", Player.RED, player);
    }

    @Test
    public void shouldHaveAnSettlerAt4_3() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(4,3));
        String type = unit.getTypeString();
        assertEquals("Unit at (4,3) should have an type of settler", GameConstants.SETTLER , type);

    }

    @Test (expected = KeyException.class) // Test if
    public void shouldNotBeAbleToAddUnitToSamePosition() throws IllegalPositionException, KeyException{

        game.addUnit(new Position(5,5), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
        game.addUnit(new Position(5,5), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        assertEquals("Unit should be of type archer", GameConstants.ARCHER, game.getUnitAt(new Position(5,5)).getTypeString());
    }

    @Test
    public void shouldNotMoveIfMoveCountIsZero() throws IllegalPositionException {

        unit = game.getUnitAt(new Position(2,0));
        unit.move();
        moveBool = game.moveUnit(new Position(2,0), new Position(2,1));
        assertFalse("Should not move if moveCount is 0", moveBool);

    }

    @Test
    public void shouldBeAbleToMove1Column() throws IllegalPositionException{

        moveBool = game.moveUnit(new Position(2,0), new Position(3,0));
        assertTrue("Should be able to move from position (2,0) to position (3,0)", moveBool);
    }

    @Test
    public void shouldBeAbleToMove1ColumnAnd1Row() throws IllegalPositionException{

        moveBool = game.moveUnit(new Position(2, 0), new Position(3, 1));
        assertTrue("Should be able to move from position (2,0) to position (3,1)", moveBool);
    }

    @Test
    public void shouldFailIfMoveMoreThan1ColumnOr1Row() throws IllegalPositionException{

        moveBool = game.moveUnit(new Position(2,0), new Position(4,2));
        assertFalse("Should not be able to move from position (2,0) to position (4,2)", moveBool);
    }

   @Test
   public void shouldNotBeAbleToMoveAnotherPlayerUnit() throws IllegalPositionException {

       moveBool = game.moveUnit(new Position(3,2), new Position(3,3));
       assertFalse("Should return false if red tries to move blue's unit", moveBool);
   }

    @Test
    public void shouldNotBeAbleToMoveOverMountainsOrOceansOrHills() throws IllegalPositionException{

        moveBool = game.moveUnit(new Position(2,0), new Position(1,0));
        assertFalse("Should not be able to move to a non-plain tile.", moveBool);
    }

    @Test
    public void shouldNotBeAbleToMoveUnitToSelfOwnedOccupiedTile() throws IllegalPositionException, KeyException{

        game.addUnit(new Position(4,4), new UnitImpl(Player.RED, GameConstants.ARCHER));
        moveBool = game.moveUnit(new Position(4,3), new Position(4,4));
        assertFalse("Should not be able move unit to self-owned occupied tile", moveBool);
    }

    /* TESTS OF BATTLE */

    @Test
     public void shouldTestThatTheRedPlayerWinsTheBattle() throws IllegalPositionException {

        moveBool = game.moveUnit(new Position(4,3), new Position(3,2));
        assertTrue("Should make the current player the winner of the battle", moveBool);
        player = game.getUnitAt(new Position(3,2)).getOwner();
        assertEquals("The unit left should be owned by player red", Player.RED, player);
    }

    @Test
    public void shouldTestThatTheBluePlayerWinsTheBattle() throws IllegalPositionException {

        game.endOfTurn();
        moveBool = game.moveUnit(new Position(3,2),new Position(4,3));
        assertTrue("Should make the current player the winner of the battle", moveBool);
        player = game.getUnitAt(new Position(4,3)).getOwner();
        assertEquals("The unit left should be owned by player blue", Player.BLUE, player);
    }

    @Test
    public void shouldSpawnUnitArcherAtPositionAt4_1() throws IllegalPositionException{
        ((CityImpl) game.getCityAt(new Position(4,1))).setProductionType(GameConstants.ARCHER);
        HelperMethods.endOfNRounds(game, 2);
        int productionSize = ((CityImpl) game.getCityAt(new Position(4,1))).getProductionSize();
        assertEquals("Should return a production size of 2", 2, productionSize);
        String type = game.getUnitAt(new Position(4,1)).getTypeString();
        assertEquals("There should be an archer at position 4,1", GameConstants.ARCHER, type);
    }

    @Test
    public void shouldSpawnUnitLegionAtPositionAt4_2() throws IllegalPositionException, KeyException{
        ((CityImpl) game.getCityAt(new Position(4,1))).setProductionType(GameConstants.LEGION);
        game.addUnit(new Position(4, 1), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
        game.addUnit(new Position(3, 1), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        HelperMethods.endOfNRounds(game, 3);

        int productionSize = ((CityImpl) game.getCityAt(new Position(4,1))).getProductionSize();
        assertEquals("Should return a production size of 3", 3, productionSize);
        String type = game.getUnitAt(new Position(4,2)).getTypeString();
        assertEquals("There should be an archer at position 4,1", GameConstants.LEGION, type);
    }


}