package hotciv.all;

import hotciv.common.GameImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;
import hotciv.variants.factories.AlphaCivFactory;
import hotciv.variants.decorators.GameDecorator;

import hotciv.variants.decorators.GameSwitchDecorator;
import org.junit.*;
import static org.junit.Assert.*;

public class TestDecorators {

    private GameImpl game;
    private GameDecorator gameDecorator;

    @Before
    public void setup() throws IllegalPositionException{
        game = new GameImpl(new AlphaCivFactory());
        gameDecorator = new GameDecorator(game);

    }

    /* UNIT TESTS */

    @Test
    public void shouldBeAbleToMoveUnit() throws IllegalPositionException{
        boolean hasMoved = gameDecorator.moveUnit(new Position(2,0), new Position(2,1));
        assertTrue("Should return true if the unit has moved", hasMoved);
    }

    @Test
    public void shouldBeAbleToEndTurn(){
        gameDecorator.endOfTurn();
        Player player = gameDecorator.getPlayerInTurn();
        assertEquals("Should return blue as player in turn", Player.BLUE, player);
    }

    @Test
    public void shouldBeAbleToChangeProduction() throws IllegalPositionException{
        String production = gameDecorator.getCityAt(new Position(4,1)).getProduction();
        assertEquals("Should produce archers", GameConstants.ARCHER, production);

        gameDecorator.changeProductionInCityAt(new Position(4,1), GameConstants.LEGION);
        production = gameDecorator.getCityAt(new Position(4,1)).getProduction();
        assertEquals("Should now have a production of legions", GameConstants.LEGION, production);
    }


    @Test
    public void shouldReturnRedAsWinner() throws IllegalPositionException{
        Player winner = gameDecorator.getWinner();
        assertNull("There should not be a winner within the first years", winner);

        HelperMethods.endOfNRounds(gameDecorator, 10);
        winner = gameDecorator.getWinner();
        assertEquals("The winner should be red at year 3000 BC", Player.RED, winner);
    }

    /* INTEGRATION TESTS */

    @Test
    public void shouldTestGameSwitch() throws IllegalPositionException{
        GameSwitchDecorator gameSwitchDecorator = new GameSwitchDecorator(game);
        gameSwitchDecorator.moveUnit(new Position(2,0), new Position(2,1));

        gameSwitchDecorator.changeState();
        HelperMethods.endOfNRounds(gameSwitchDecorator, 1);
        gameSwitchDecorator.moveUnit(new Position(2,1), new Position(3,1));
    }

}
