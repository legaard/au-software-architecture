package hotciv.all;

import hotciv.framework.*;
import hotciv.common.*;
import hotciv.exceptions.IllegalPositionException;
import hotciv.variants.FractalGeneratorAdaptor;
import hotciv.variants.factories.AlphaCivFactory;

public class TestFractalGeneratorAdaptor {

    public static void main(String[] arg) throws IllegalPositionException{
        Game game = new GameImpl(new AlphaCivFactory());

        new FractalGeneratorAdaptor().createWorld(game);

    }


}
