package hotciv.all;

import hotciv.common.GameImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;


import hotciv.variants.factories.DeltaCivFactory;
import hotciv.variants.strategies.*;
import org.junit.*;

import java.security.KeyException;

import static org.junit.Assert.*;

public class TestDeltaCiv {

    private GameImpl game;
    private City city;
    private Player player;

    @Before
    public void setup() throws IllegalPositionException, KeyException {

        game = new GameImpl(new DeltaCivFactory());
    }

    /* TEST FOR CITIES */

    @Test
    public void shouldHaveARedCityAt8_12() throws IllegalPositionException {

        city = game.getCityAt(new Position(8, 12));
        assertNotNull("Should have a city at position (8,12)", city);
        player = city.getOwner();
        assertEquals("Owner of the city on position (8,12) should be red", Player.RED, player);

    }

    @Test
    public void shouldHaveABlueCityAt4_5() throws IllegalPositionException {

        city = game.getCityAt(new Position(4,5));
        assertNotNull("Should have a city at position (4,5)", city);
        player = city.getOwner();
        assertEquals("Owner of the city on position (4,5) should be blue", Player.BLUE, player);
    }

    /* TEST FOR TILES */

    @Test
    public void shouldHaveAnOceanTileAt2_12() throws IllegalPositionException {

        String tileType = game.getTileAt(new Position(2,12)).getTypeString();
        assertEquals("Should have an ocean on position (2,12)", GameConstants.OCEANS, tileType);
    }

    @Test
    public void shouldHaveAMountainTileAt0_5() throws IllegalPositionException {
        String tileType = game.getTileAt(new Position(0,5)).getTypeString();
        assertEquals("Should have a mountain on position (0,5)", GameConstants.MOUNTAINS, tileType);
    }

    @Test
    public void shouldHaveAForestTileAt9_1() throws IllegalPositionException {

        String tileType = game.getTileAt(new Position(9,1)).getTypeString();
        assertEquals("Should have a forest on position (9,1)", GameConstants.FOREST, tileType);
    }

    @Test
    public void shouldHaveAPlainTileAt14_3() throws IllegalPositionException {

        String tileType = game.getTileAt(new Position(14,3)).getTypeString();
        assertEquals("Should have a forest on position (14,3)", GameConstants.PLAINS, tileType);
    }
}
