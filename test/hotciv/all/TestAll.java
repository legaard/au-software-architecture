package hotciv.all;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestAlphaCiv.class,
        TestBetaCiv.class,
        TestGammaCiv.class,
        TestDeltaCiv.class,
        TestEpsilonCiv.class,
        TestZetaCiv.class,
        TestThetaCiv.class,
        TestDecorators.class,
        TestObservers.class
})
public class TestAll{

}

