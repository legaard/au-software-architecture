package hotciv.all;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.common.UnitImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

import hotciv.variants.factories.GammaCivFactory;
import hotciv.variants.factories.ThetaCivFactory;
import hotciv.variants.decorators.ChariotDecorator;
import hotciv.variants.strategies.GammaCivUnitActionStrategy;
import org.junit.*;

import java.security.KeyException;

import static org.junit.Assert.*;

public class TestThetaCiv {

    private GameImpl game;

    @Before
    public void setup() throws IllegalPositionException{
        game = new GameImpl(new GammaCivFactory());
    }

    @Test
    public void shouldReturnProductionOfCityToChariot() throws IllegalPositionException, KeyException{
        game.addCity(new Position(1,1), new CityImpl(Player.RED));
        ((CityImpl) game.getCityAt(new Position(1,1))).setProductionType(GameConstants.CHARIOT);
        String productionType = game.getCityAt(new Position(1,1)).getProduction();
        assertEquals("Should return chariot as city production", GameConstants.CHARIOT, productionType);
    }

    @Test
    public void shouldCheckFunctionalityOfTheChariotDecorator() throws IllegalPositionException, KeyException{
        game.addUnit(new Position(6,6), new UnitImpl(Player.BLUE, GameConstants.CHARIOT));
        ChariotDecorator chariotDecorator = new ChariotDecorator(new GammaCivUnitActionStrategy());

        int defensiveStrengthBefore = game.getUnitAt(new Position(6, 6)).getDefensiveStrength();
        assertEquals("Should have a defensive strengtg of 1", GameConstants.CHARIOT_DEFENSE_STRENGTH, defensiveStrengthBefore);
        chariotDecorator.performUnitAction(game, new Position(6,6));

        int defensiveStrengthAfter = game.getUnitAt(new Position(6, 6)).getDefensiveStrength();
        assertEquals("The defense strength should be doubled", GameConstants.CHARIOT_DEFENSE_STRENGTH * 2,defensiveStrengthAfter);

    }

    @Test
    public void shouldCheckIntegration() throws IllegalPositionException, KeyException {
        GameImpl thetaGame = new GameImpl(new ThetaCivFactory());
        thetaGame.addUnit(new Position(6,6), new UnitImpl(Player.RED, GameConstants.CHARIOT));

        int defensiveStrengthBefore = thetaGame.getUnitAt(new Position(6, 6)).getDefensiveStrength();
        assertEquals("Should have a defensive strengtg of 1", GameConstants.CHARIOT_DEFENSE_STRENGTH, defensiveStrengthBefore);
        thetaGame.performUnitActionAt(new Position(6,6));

        int defensiveStrengthAfter = thetaGame.getUnitAt(new Position(6, 6)).getDefensiveStrength();
        assertEquals("The defense strength should be doubled", GameConstants.CHARIOT_DEFENSE_STRENGTH * 2,defensiveStrengthAfter);

        boolean moveable = ((UnitImpl) thetaGame.getUnitAt(new Position(6, 6))).isMoveable();
        assertFalse("The unit should not be moveable", moveable);

    }


}
