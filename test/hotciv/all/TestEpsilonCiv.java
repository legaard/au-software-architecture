package hotciv.all;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.common.TileImpl;
import hotciv.common.UnitImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

import hotciv.variants.factories.EpsilonCivFactory;
import hotciv.variants.strategies.*;
import org.junit.*;
import static org.junit.Assert.*;

import java.security.KeyException;

public class TestEpsilonCiv {

    private GameImpl game;
    private EpsilonCivBattleStrategy epsilonCivBattleStrategy;

    @Before
    public void setup() throws IllegalPositionException, KeyException{
        game = new GameImpl(new EpsilonCivFactory());

        //
        epsilonCivBattleStrategy = new EpsilonCivBattleStrategy(new FixFactorStrategy(2));

        // Player Red setup
        game.addCity(new Position(12,12), new CityImpl(Player.RED));
        game.addUnit(new Position(12,12), new UnitImpl(Player.RED, GameConstants.ARCHER));
        game.addUnit(new Position(11,13), new UnitImpl(Player.RED, GameConstants.LEGION));

        // Player blue setup
        game.addUnit(new Position(13,11), new UnitImpl(Player.BLUE, GameConstants.LEGION));
        game.addUnit(new Position(13,10), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
        game.addUnit(new Position(14,11), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        game.addTile(new Position(13,11), new TileImpl(GameConstants.HILLS));

    }

    @Test
    public void shouldReturnTheCorrectDefenseAndAttackStrength() throws  IllegalPositionException, KeyException {
        game.addUnit(new Position(10,10), new UnitImpl(Player.RED, GameConstants.ARCHER));
        game.addUnit(new Position(11,10), new UnitImpl(Player.RED, GameConstants.LEGION));
        game.addUnit(new Position(12,10), new UnitImpl(Player.RED, GameConstants.SETTLER));

        // ARCHER defense and attack strength
        int archerAttackStrength = game.getUnitAt(new Position(10,10)).getAttackingStrength();
        int archerDefenseStrength = game.getUnitAt(new Position(10,10)).getDefensiveStrength();
        assertEquals("Should return attack strength 2", 2, archerAttackStrength);
        assertEquals("Should return defense strength 2", 3, archerDefenseStrength);

        // LEGION defense and attack strength
        int legionAttackStrength = game.getUnitAt(new Position(11,10)).getAttackingStrength();
        int legionDefenseStrength = game.getUnitAt(new Position(11,10)).getDefensiveStrength();
        assertEquals("Should return attack strength 4", 4, legionAttackStrength);
        assertEquals("Should return defense strength 2", 2, legionDefenseStrength);

        // SETTLER defense and attack strength
        int settlerAttackStrength = game.getUnitAt(new Position(12,10)).getAttackingStrength();
        int settlerDefenseStrength = game.getUnitAt(new Position(12,10)).getDefensiveStrength();
        assertEquals("Should return attack strength 0", 0, settlerAttackStrength);
        assertEquals("Should return defense strength 3", 3, settlerDefenseStrength);
    }

    @Test
    public void shouldReturnABattleStrengthOf24() throws IllegalPositionException{
        int attackStrength = epsilonCivBattleStrategy.calculateBattleStrength(game.getUnitAt(new Position(13,11)).getAttackingStrength(),
                game, Player.BLUE, new Position(13,11));
        assertEquals("Should return an attacking strength of 24", ((4+1+1)*2*2), attackStrength);
    }

    @Test
    public void shouldReturnBattleStrengthOf24() throws IllegalPositionException {
        int defenseStrength = epsilonCivBattleStrategy.calculateBattleStrength(game.getUnitAt(new Position(12,12)).getDefensiveStrength(),
                game, Player.RED, new Position(12,12));
        assertEquals("Should return an defense strength of 24", ((3+1)*3*2), defenseStrength);
    }

    @Test
    public void shouldReturnRedArcherAsBattleWinner() throws IllegalPositionException {
        boolean bool = epsilonCivBattleStrategy.battle(game, new Position(13,11), new Position(12,12));
        assertFalse("Should return red Archer as battle winner", bool);
    }

    @Test
    public void shouldReturnRedPlayerAsWinner() throws IllegalPositionException, KeyException {
        game.addUnit(new Position(16,16), new UnitImpl(Player.RED, GameConstants.LEGION));
        game.addUnit(new Position(15,16), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
        game.addUnit(new Position(14,16), new UnitImpl(Player.BLUE, GameConstants.ARCHER));
        game.addUnit(new Position(13,16), new UnitImpl(Player.BLUE, GameConstants.ARCHER));

        game.moveUnit(new Position(16,16), new Position(15,16));
        HelperMethods.endOfNRounds(game,2);
        game.moveUnit(new Position(15,16), new Position(14,16));
        HelperMethods.endOfNRounds(game,2);
        game.moveUnit(new Position(14,16), new Position(13,16));
        HelperMethods.endOfNRounds(game,2);

        Player player = game.getUnitAt(new Position(13,16)).getOwner();
        assertEquals("Should return red player as winner of the game", Player.RED, player);
    }
}