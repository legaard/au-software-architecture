package hotciv.all;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.common.UnitImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;

import hotciv.variants.factories.BetaCivFactory;
import hotciv.variants.strategies.*;
import org.junit.*;
import static org.junit.Assert.*;

import java.security.KeyException;

public class TestBetaCiv {

    private GameImpl game;
    private Player player;

    @Before
    public void setup() throws IllegalPositionException {

        game = new GameImpl(new BetaCivFactory());
    }

    @Test
    public void shouldReturnWinnerThatFirstConquersAllCities() throws IllegalPositionException, KeyException {
        game.endOfTurn(); // End turn to make sure it is blue's turn - otherwise conquer isn't possible
        game.addUnit(new Position(1,1), new UnitImpl(Player.RED, GameConstants.ARCHER));
        game.conquerCity( (CityImpl) game.getCityAt(new Position(1,1)), new Position(1,0), new Position(1,1) );
        player = game.getWinner();
        assertEquals("Should return blue as the winner", Player.BLUE, player);
    }

    @Test
    public void shouldConquerBlueCity() throws IllegalPositionException, KeyException{

        game.addCity(new Position(7,7), new CityImpl(Player.BLUE));
        game.addUnit(new Position(6,6), new UnitImpl(Player.RED, GameConstants.ARCHER));
        game.moveUnit(new Position(6,6), new Position(7,7));
        player = game.getCityAt(new Position(7,7)).getOwner();
        assertEquals("Should be conquer red unit", Player.RED, player);
    }

    /* Tests for BetaAgingStrategy */

    @Test
    public void shouldReturnYear1BC(){

        HelperMethods.endOfNRounds(game, 40);
        int year1BC = game.getAge();
        assertEquals("Should return year -1", -1, year1BC);
    }

    @Test
    public void shouldReturnYear100BC() {

        HelperMethods.endOfNRounds(game, 39);
        int year100BC = game.getAge();
        assertEquals("Should return year -100", -100, year100BC);
    }

    @Test
    public void shouldReturn1AD() {

        HelperMethods.endOfNRounds(game, 41);
        int year1AD = game.getAge();
        assertEquals("Should return year 1", 1, year1AD);
    }

    @Test
    public void shouldReturn50AD() {

        HelperMethods.endOfNRounds(game, 42);
        int year50AD = game.getAge();
        assertEquals("Should return year 50", 50, year50AD);
    }

    @Test
    public void shouldReturn100AD() {

        HelperMethods.endOfNRounds(game, 43);
        int year100AD = game.getAge();
        assertEquals("Should return year 100", 100, year100AD);
    }

    @Test
    public void shouldReturn150AD() {

        HelperMethods.endOfNRounds(game, 44);
        int year150AD = game.getAge();
        assertEquals("Should return year 200", 150, year150AD);
    }

    @Test
    public void shouldReturn1750AD() {

        HelperMethods.endOfNRounds(game, 76);
        int year1750AD = game.getAge();
        assertEquals("Should return year 1750", 1750, year1750AD);
    }

    @Test
    public void shouldReturn1800AD() {

        HelperMethods.endOfNRounds(game, 78);
        int year1800AD = game.getAge();
        assertEquals("Should return year 1800", 1800, year1800AD);
    }

    @Test
    public void shouldReturn1905AD() {

        HelperMethods.endOfNRounds(game, 83);
        int year1905AD = game.getAge();
        assertEquals("Should return year 1905", 1905, year1905AD);
    }

    @Test
    public void shouldreturn1975AD() {

        HelperMethods.endOfNRounds(game, 101);
        int year1975AD = game.getAge();
        assertEquals("Should return year 1975", 1975, year1975AD);
    }
}