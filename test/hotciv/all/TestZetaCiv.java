package hotciv.all;

import hotciv.common.GameImpl;
import hotciv.common.UnitImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.variants.factories.ZetaCivFactory;
import org.junit.*;

import java.security.KeyException;

import static org.junit.Assert.*;

public class TestZetaCiv {

    GameImpl game;
    Player player;

    @Before
    public void setup() throws IllegalPositionException, KeyException{
        game = new GameImpl(new ZetaCivFactory());
    }

    @Test
    public void shouldReturnRedAsWinnerAfterConquerAllCities() throws IllegalPositionException, KeyException{
        player = game.getWinner();
        assertNull("There should be no winner yet", player);

        game.addUnit(new Position(4,0), new UnitImpl(Player.RED, GameConstants.ARCHER));
        game.moveUnit(new Position(4,0), new Position(4,1));
        player = game.getWinner();
        assertEquals("The Winner should be red", Player.RED, player);
    }

    @Test
    public void shouldReturnRedAfter20RoundsAndEliminating3BlueUnits() throws IllegalPositionException, KeyException{
        player = game.getWinner();
        assertNull("There should be no winner yet", player);

        game.addUnit(new Position(3,0), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        game.addUnit(new Position(4,0), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        game.addUnit(new Position(5,0), new UnitImpl(Player.BLUE, GameConstants.SETTLER));
        HelperMethods.endOfNRounds(game, 21);

        game.moveUnit(new Position(2, 0), new Position(3, 0));
        HelperMethods.endOfNRounds(game, 1);

        game.moveUnit(new Position(3, 0), new Position(4, 0));
        HelperMethods.endOfNRounds(game, 1);

        game.moveUnit(new Position(4, 0), new Position(5, 0));

        player = game.getWinner();
        assertEquals("The winner should be red", Player.RED, player);
    }
}
