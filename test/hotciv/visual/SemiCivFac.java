package hotciv.visual;

import hotciv.common.GameImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.Game;
import hotciv.tools.*;
import hotciv.variants.factories.SemiCivFactory;
import minidraw.framework.*;
import minidraw.standard.MiniDrawApplication;

import javax.swing.*;

public class SemiCivFac{
    public static void main(String[] args) throws IllegalPositionException {
        Game game = new GameImpl(new SemiCivFactory());

        DrawingEditor editor =
                new MiniDrawApplication( "Shift-Click unit to invoke its action",
                        new HotCivFactory4(game) );
        editor.open();
        editor.showStatus("Shift-Click on unit to see Game's performAction method being called.");

        CompositeTool compositeTool = new CompositeTool();
        compositeTool.addTool(new ActionTool(game));
        compositeTool.addTool(new EndOfTurnTool(game));
        compositeTool.addTool(new SetFocusTool(editor,game));
        compositeTool.addTool(new UnitMoveTool(editor, game));
        editor.setTool( compositeTool );
    }
}
