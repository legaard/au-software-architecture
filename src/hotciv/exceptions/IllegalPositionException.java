package hotciv.exceptions;

public class IllegalPositionException extends Exception{

    public IllegalPositionException(String e){
        super(e);
    }

}
