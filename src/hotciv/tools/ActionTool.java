package hotciv.tools;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import hotciv.view.GfxConstants;
import minidraw.standard.NullTool;

import java.awt.event.MouseEvent;

/**
 * Created by lasse_legaard on 15/12/14.
 */
public class ActionTool extends NullTool{

    Game game;

    public ActionTool(Game game) {
        this.game = game;
    }

    public void mouseDown(MouseEvent e, int x, int y) {
            if(e.isShiftDown() && game.getUnitAt(GfxConstants.getPositionFromXY(x,y)) != null){
                game.performUnitActionAt(GfxConstants.getPositionFromXY(x,y));
            }
    }
}
