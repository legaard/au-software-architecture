package hotciv.tools;

import hotciv.framework.Game;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.standard.SelectionTool;

import java.awt.event.MouseEvent;

public class SetFocusTool extends SelectionTool{

    Game game;

    public SetFocusTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;
    }

    public void mouseDown(MouseEvent e, int x, int y) {
        game.setTileFocus(GfxConstants.getPositionFromXY(x,y));
    }
}
