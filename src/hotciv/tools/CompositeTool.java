package hotciv.tools;

import hotciv.framework.Game;
import minidraw.framework.Tool;
import minidraw.standard.NullTool;

import java.awt.event.MouseEvent;
import java.util.ArrayList;


public class CompositeTool extends NullTool{

    private ArrayList<Tool> toolArrayList = new ArrayList<Tool>();

    @Override
    public void mouseDown(MouseEvent e, int x, int y) {
        for (Tool tool : toolArrayList) {
            tool.mouseDown(e,x,y);
        }
    }

    public void addTool(Tool tool){
        toolArrayList.add(tool);
    }

    public void removeTool(Tool tool){
        toolArrayList.remove(tool);
    }


}
