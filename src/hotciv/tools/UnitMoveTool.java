package hotciv.tools;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.Game;
import hotciv.framework.Position;
import hotciv.framework.Unit;
import hotciv.view.GfxConstants;
import minidraw.framework.DrawingEditor;
import minidraw.standard.SelectionTool;

import java.awt.event.MouseEvent;


public class UnitMoveTool extends SelectionTool{

    Game game;
    boolean selected = false;
    Position from;

    public UnitMoveTool(DrawingEditor editor, Game game) {
        super(editor);
        this.game = game;
    }

    public void mouseDown(MouseEvent e, int x, int y) {
        if(!selected){
            from = GfxConstants.getPositionFromXY(x,y);
            Unit u = game.getUnitAt(from);
            if (u != null){
                selected = true;
            }

        } else {
            game.moveUnit(from, GfxConstants.getPositionFromXY(x,y));
            selected = false;

        }
    }
}
