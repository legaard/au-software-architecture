package hotciv.tools;

import hotciv.framework.Game;
import hotciv.view.GfxConstants;
import minidraw.standard.NullTool;

import java.awt.event.MouseEvent;

/**
 * Created by lasse_legaard on 15/12/14.
 */
public class EndOfTurnTool extends NullTool {

    Game game;

    public EndOfTurnTool(Game game) {
        this.game = game;
    }


    public void mouseDown(MouseEvent e, int x, int y) {
        if ((x > GfxConstants.TURN_SHIELD_X && x < (GfxConstants.TURN_SHIELD_X+27)) &&
                (y > GfxConstants.TURN_SHIELD_Y && y < (GfxConstants.TURN_SHIELD_Y+ 39)) ){
            game.endOfTurn();
        }
    }
}
