package hotciv.variants.strategies;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.Position;
import hotciv.framework.WinnerStrategy;

import java.util.HashMap;

public class BetaCivWinnerStrategy implements WinnerStrategy {

    public Player checkWinner(Game game) {

        HashMap<Position, CityImpl> cityHashMap = (HashMap) ((GameImpl) game).getCityHashMap();

        //Get owner of the first city
        Player player = cityHashMap.values().iterator().next().getOwner();

        for (CityImpl city : cityHashMap.values()){
            if(player != city.getOwner()){
                return null;
            }
        }
        return player;
    }
}
