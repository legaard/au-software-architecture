package hotciv.variants.strategies;

import hotciv.common.GameImpl;
import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

import java.util.ArrayList;
import java.util.Iterator;

public class EpsilonCivBattleStrategy implements BattleStrategy {

    private FactorStrategy factorStrategy;

    public EpsilonCivBattleStrategy(FactorStrategy factorStrategy) {
        this.factorStrategy = factorStrategy;
    }

    public boolean battle(Game g, Position from, Position to) {

        GameImpl game = (GameImpl) g;

        int attackingUnitStrength = game.getUnitAt(from).getAttackingStrength();
        attackingUnitStrength = calculateBattleStrength(attackingUnitStrength, game, game.getPlayerInTurn(), from);

        int defendingUnitStrength = game.getUnitAt(to).getDefensiveStrength();
        defendingUnitStrength = calculateBattleStrength(defendingUnitStrength, game, game.getUnitAt(to).getOwner(), to);


        return attackingUnitStrength > defendingUnitStrength;
    }

    public int calculateBattleStrength(int unitStrength, Game game, Player player, Position position){
        int friendlySupport = getFriendlySupport(game, position, player);
        int terrainFactor = getTerrainFactor(game, position);

        return ((unitStrength + friendlySupport) * terrainFactor) * factorStrategy.randomFactor();
    }


    public static int getTerrainFactor(Game game, Position position) {

        Tile t = null;

        try {
            t = game.getTileAt(position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ( game.getCityAt(position) != null ) { return 3; }

        if ( t.getTypeString().equals(GameConstants.FOREST) ||
                t.getTypeString().equals(GameConstants.HILLS) ) {
            return 2;
        }
        return 1;
    }

    public static Iterator<Position> get8NeighborhoodIterator(Position center){
        ArrayList<Position> list = new ArrayList<Position>();
        int row = center.getRow(); int col = center.getColumn();
        int r,c;
        for (int dr = -1; dr <= +1; dr++) {
            for (int dc = -1; dc <= +1; dc++) {
                r = row+dr; c = col+dc;
                // avoid positions outside the world
                if ( r >= 0 && r < GameConstants.WORLDSIZE &&
                        c >= 0 && c < GameConstants.WORLDSIZE ) {
                    // avoid center position
                    if ( r != row || c != col ){
                        try {
                            list.add(new Position(r,c));
                        } catch (IllegalPositionException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return list.iterator();
    }

    public static int getFriendlySupport(Game game, Position position,
                                         Player player) {
        Iterator<Position> neighborhood = get8NeighborhoodIterator(position);
        Position p;
        int support = 0;
        while ( neighborhood.hasNext() ) {
            p = neighborhood.next();
            if ( game.getUnitAt(p) != null &&
                    game.getUnitAt(p).getOwner() == player ) {
                support++;
            }
        }
        return support;
    }
}