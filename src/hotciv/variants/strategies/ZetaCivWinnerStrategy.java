package hotciv.variants.strategies;

import hotciv.common.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.WinnerStrategy;

public class ZetaCivWinnerStrategy implements WinnerStrategy {

    private WinnerStrategy epsilonCivWinnerStrategy;
    private WinnerStrategy betaCivWinnerStrategy;

    public ZetaCivWinnerStrategy(WinnerStrategy epsilonCivWinnerStrategy, WinnerStrategy betaCivWinnerStrategy) {

        this.epsilonCivWinnerStrategy = epsilonCivWinnerStrategy;
        this.betaCivWinnerStrategy = betaCivWinnerStrategy;
    }

    public Player checkWinner(Game g) {
        GameImpl game = (GameImpl) g;

        if(game.getRounds() <= 20) {
            game.resetBattleWinsHashMap();
            return betaCivWinnerStrategy.checkWinner(game);
        } else {
            return epsilonCivWinnerStrategy.checkWinner(game);
        }
    }
}
