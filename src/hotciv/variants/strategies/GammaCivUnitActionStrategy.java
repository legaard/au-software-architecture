package hotciv.variants.strategies;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.common.UnitImpl;
import hotciv.framework.Game;
import hotciv.framework.GameConstants;
import hotciv.framework.Position;
import hotciv.framework.UnitActionStrategy;

import java.security.KeyException;

public class GammaCivUnitActionStrategy implements UnitActionStrategy {

    public void performUnitAction(Game g, Position p) {

        UnitImpl unit = (UnitImpl) g.getUnitAt(p);
        GameImpl game = (GameImpl) g;

        if (unit.getTypeString().equals(GameConstants.SETTLER)) {
            try {
                game.addCity(p, new CityImpl(unit.getOwner()));
            } catch (KeyException e) {
                e.printStackTrace();
            }
            game.removeUnitAt(p);
        }

        if (unit.getTypeString().equals(GameConstants.ARCHER)) {

            if (unit.isMoveable()) {
                unit.setDefensiveStrength(unit.getDefensiveStrength() * 2);
                unit.setMoveable(false);
            } else {
                unit.setDefensiveStrength(GameConstants.ARCHER_DEFENSE_STRENGTH);
                unit.setMoveable(true);
            }
        }
    }
}
