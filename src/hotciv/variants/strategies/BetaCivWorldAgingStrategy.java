package hotciv.variants.strategies;

import hotciv.common.GameImpl;
import hotciv.framework.WorldAgingStrategy;
import hotciv.framework.Game;

public class BetaCivWorldAgingStrategy implements WorldAgingStrategy {

    public void changeAge(Game game) {

        GameImpl gameImpl = (GameImpl) game;
        int age = gameImpl.getAge();

        // Around birth of Christ
        // The sequence is -100, -1, +1, +50
        if (age >= -4000 && age < -100){
            gameImpl.setAge(gameImpl.getAge() + 100);
        } else if (age == -100){
            gameImpl.setAge(gameImpl.getAge() + 99);
        } else if (age == -1){
            gameImpl.setAge(gameImpl.getAge() + 2);
        } else if (age == 1){
            gameImpl.setAge(gameImpl.getAge() + 49);
        }

        // Between 50AD and 1750
        // 50 years pass per round.
        if (age >= 50 && age < 1750) {
            gameImpl.setAge(gameImpl.getAge() + 50);
        }

        // Between 1750 and 1900
        // 25 years pass per round.
        if (age >= 1750 && age < 1900) {
            gameImpl.setAge(gameImpl.getAge() + 25);
        }

        // Between 1900 and 1970
        // 5 years pass per round.
        if (age >= 1900 && age < 1970) {
            gameImpl.setAge(gameImpl.getAge() + 5);
        }

        // After 1970
        // 1 year per round
        if (age >= 1970) {
            gameImpl.setAge(gameImpl.getAge() + 1);
        }
    }
}
