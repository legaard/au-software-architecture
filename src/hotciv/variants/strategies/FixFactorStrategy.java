package hotciv.variants.strategies;

import hotciv.framework.FactorStrategy;

public class FixFactorStrategy implements FactorStrategy {

    private int number;

    public FixFactorStrategy(int number) {
        this.number = number;
    }

    public int randomFactor() {
        return number;
    }
}
