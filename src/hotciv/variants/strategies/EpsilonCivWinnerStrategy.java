package hotciv.variants.strategies;

import hotciv.common.GameImpl;
import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.WinnerStrategy;

import java.util.HashMap;

public class EpsilonCivWinnerStrategy implements WinnerStrategy{


    public Player checkWinner(Game g) {
        HashMap<Player, Integer> battleWinsHashMap = (HashMap<Player, Integer>) ((GameImpl) g).getBattleWinsHashMap();

        Player winner = null;

        for (Player player : battleWinsHashMap.keySet()){
            if (battleWinsHashMap.get(player) == 3){
                winner = player;
            }
        }

        return winner;
    }
}
