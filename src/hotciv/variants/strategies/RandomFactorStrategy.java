package hotciv.variants.strategies;

import hotciv.framework.FactorStrategy;

import java.util.Random;

public class RandomFactorStrategy implements FactorStrategy {

    public int randomFactor() {
        Random randomFactor = new Random();
        return randomFactor.nextInt(6) + 1;
    }
}
