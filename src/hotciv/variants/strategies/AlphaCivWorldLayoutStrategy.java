package hotciv.variants.strategies;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.common.TileImpl;
import hotciv.common.UnitImpl;
import hotciv.framework.*;

public class AlphaCivWorldLayoutStrategy implements WorldLayoutStrategy{

    public void createWorld(Game game) {

        try {
            //Add tiles to the world
            ((GameImpl) game).addTile(new Position(1,0), new TileImpl(GameConstants.OCEANS));
            ((GameImpl) game).addTile(new Position(2,2), new TileImpl(GameConstants.MOUNTAINS));
            ((GameImpl) game).addTile(new Position(0,1), new TileImpl(GameConstants.HILLS));

            //Add cities to the world
            ((GameImpl) game).addCity(new Position(4,1), new CityImpl(Player.BLUE));
            ((GameImpl) game).addCity(new Position(1,1), new CityImpl(Player.RED));

            //Add units to the world
            ((GameImpl) game).addUnit(new Position(2,0), new UnitImpl(Player.RED, GameConstants.ARCHER));
            ((GameImpl) game).addUnit(new Position(3,2), new UnitImpl(Player.BLUE, GameConstants.LEGION));
            ((GameImpl) game).addUnit(new Position(4,3), new UnitImpl(Player.RED, GameConstants.SETTLER));

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
