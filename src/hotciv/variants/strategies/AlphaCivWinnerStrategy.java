package hotciv.variants.strategies;

import hotciv.framework.Game;
import hotciv.framework.Player;
import hotciv.framework.WinnerStrategy;


public class AlphaCivWinnerStrategy implements WinnerStrategy {

    public Player checkWinner(Game game) {

        if (game.getAge() == -3000){
            return Player.RED;
        } else return null;
    }
}
