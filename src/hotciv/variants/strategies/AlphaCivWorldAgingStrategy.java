package hotciv.variants.strategies;

import hotciv.common.GameImpl;
import hotciv.framework.WorldAgingStrategy;
import hotciv.framework.Game;

public class AlphaCivWorldAgingStrategy implements WorldAgingStrategy {

    public void changeAge(Game game) {

        // Increase year by 100 each round
        ((GameImpl)game).setAge(game.getAge() + 100);
    }
}
