package hotciv.variants.strategies;

import hotciv.common.CityImpl;
import hotciv.common.GameImpl;
import hotciv.framework.*;


public class DeltaCivWorldLayoutStrategy implements WorldLayoutStrategy{

    public void createWorld(Game game) {

        // String array used for world layout
        String[] layout = new String[] {
                "...ooMooooo.....",
                "..ohhoooofffoo..",
                ".oooooMooo...oo.",
                ".ooMMMoooo..oooo",
                "...ofooohhoooo..",
                ".ofoofooooohhoo.",
                "...ooo..........",
                ".ooooo.ooohooM..",
                ".ooooo.oohooof..",
                "offfoooo.offoooo",
                "oooooooo...ooooo",
                ".ooMMMoooo......",
                "..ooooooffoooo..",
                "....ooooooooo...",
                "..ooohhoo.......",
                ".....ooooooooo.."
        };

        ((GameImpl) game).setWorldLayout(layout);

        try {
            ((GameImpl) game).addCity(new Position(8, 12), new CityImpl(Player.RED));
            ((GameImpl) game).addCity(new Position(4, 5), new CityImpl(Player.BLUE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

