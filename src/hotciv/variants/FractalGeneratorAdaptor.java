package hotciv.variants;

import hotciv.common.GameImpl;
import hotciv.framework.*;
import thirdparty.ThirdPartyFractalGenerator;

public class FractalGeneratorAdaptor implements WorldLayoutStrategy{

    private ThirdPartyFractalGenerator thirdPartyFractalGenerator = new ThirdPartyFractalGenerator();

    public void createWorld(Game game) {
        ((GameImpl) game).setWorldLayout(createStringArray());
    }


    public String[] createStringArray(){
        String[] layout = new String[16];

        for (int r = 0; r < GameConstants.WORLDSIZE; r++){

            String line = "";
            for (int c = 0; c < GameConstants.WORLDSIZE; c++){

                char l = thirdPartyFractalGenerator.getLandscapeAt(r,c);
                line += l;
            }

            layout[r] = line;
        }

        for (int i = 0; i < layout.length; i++){
            System.out.println(layout[i].toString());
        }

        return layout;
    }

}
