package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.decorators.ChariotDecorator;
import hotciv.variants.strategies.*;

public class ThetaCivFactory implements GameFactory{
    @Override
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new DeltaCivWorldLayoutStrategy();
    }

    @Override
    public WorldAgingStrategy createWorldAgingStrategy() {
        return new BetaCivWorldAgingStrategy();
    }

    @Override
    public UnitActionStrategy createUnitActionStrategy() {
        return new ChariotDecorator(new GammaCivUnitActionStrategy());
    }

    @Override
    public BattleStrategy createBattleStrategy() {
        return new AlphaCivBattleStrategy();
    }

    @Override
    public WinnerStrategy createWinnerStrategy() {
        return new BetaCivWinnerStrategy();
    }
}
