package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class EpsilonCivFactory implements GameFactory {
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new AlphaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new AlphaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new EpsilonCivBattleStrategy(new FixFactorStrategy(2));
    }

    public WinnerStrategy createWinnerStrategy() {
        return new EpsilonCivWinnerStrategy();
    }
}
