package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class SemiCivFactory implements GameFactory {

    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new DeltaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new BetaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new GammaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new EpsilonCivBattleStrategy(new RandomFactorStrategy());
    }

    public WinnerStrategy createWinnerStrategy() {
        return new EpsilonCivWinnerStrategy();
    }
}
