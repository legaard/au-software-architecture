package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class DeltaCivFactory implements GameFactory {
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new DeltaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new AlphaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new AlphaCivBattleStrategy();
    }

    public WinnerStrategy createWinnerStrategy() {
        return new AlphaCivWinnerStrategy();
    }
}
