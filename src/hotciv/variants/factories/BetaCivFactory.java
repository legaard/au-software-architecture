package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class BetaCivFactory implements GameFactory {
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new AlphaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new BetaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new AlphaCivBattleStrategy();
    }

    public WinnerStrategy createWinnerStrategy() {
        return new BetaCivWinnerStrategy();
    }
}
