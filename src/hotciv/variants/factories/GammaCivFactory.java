package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class GammaCivFactory implements GameFactory {
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new DeltaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new BetaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new GammaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new AlphaCivBattleStrategy();
    }

    public WinnerStrategy createWinnerStrategy() {
        return new BetaCivWinnerStrategy();
    }
}
