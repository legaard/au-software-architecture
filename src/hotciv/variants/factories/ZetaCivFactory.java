package hotciv.variants.factories;

import hotciv.framework.*;
import hotciv.variants.strategies.*;

public class ZetaCivFactory implements GameFactory {
    public WorldLayoutStrategy createWorldLayoutStrategy() {
        return new AlphaCivWorldLayoutStrategy();
    }

    public WorldAgingStrategy createWorldAgingStrategy() {
        return new AlphaCivWorldAgingStrategy();
    }

    public UnitActionStrategy createUnitActionStrategy() {
        return new AlphaCivUnitActionStrategy();
    }

    public BattleStrategy createBattleStrategy() {
        return new AlphaCivBattleStrategy();
    }

    public WinnerStrategy createWinnerStrategy() {
        return new ZetaCivWinnerStrategy(new EpsilonCivWinnerStrategy(), new BetaCivWinnerStrategy());
    }
}
