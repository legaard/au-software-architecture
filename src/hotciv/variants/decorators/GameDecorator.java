package hotciv.variants.decorators;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

public class GameDecorator implements Game {

    private Game game;

    public GameDecorator(Game game) {
        this.game = game;
    }

    public Tile getTileAt(Position p) throws IllegalPositionException {
        return game.getTileAt(p);
    }

    public Unit getUnitAt(Position p) {
        return game.getUnitAt(p);
    }

    public City getCityAt(Position p) {
        return game.getCityAt(p);
    }

    public Player getPlayerInTurn() {
        return game.getPlayerInTurn();
    }

    public Player getWinner() {

        if(game.getWinner() != null){
            System.out.println(game.getWinner() + " wins the game");
        }

        return game.getWinner();
    }

    public int getAge() {
        return game.getAge();
    }

    public boolean moveUnit(Position from, Position to) {

        boolean hasMoved = game.moveUnit(from, to);

        if(hasMoved){
            System.out.println(game.getPlayerInTurn() + " moves "
                    + game.getUnitAt(to).getTypeString() + " from "
                    + from + " to " + to + ".");
        }

        return hasMoved;
    }

    public void endOfTurn() {
        System.out.println(game.getPlayerInTurn() + " ends turn.");
        game.endOfTurn();
    }

    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        System.out.println("This function is not implemented yet");
        game.changeWorkForceFocusInCityAt(p, balance);
    }

    public void changeProductionInCityAt(Position p, String unitType) {
        System.out.println(game.getCityAt(p).getOwner() + " changes production in city at " +
                p + " to " + unitType + ".");

        game.changeProductionInCityAt(p, unitType);
    }

    public void performUnitActionAt(Position p) {
        System.out.println(game.getUnitAt(p).getOwner() + " performs action at " + p + ".");
        game.performUnitActionAt(p);
    }

    @Override
    public void addObserver(GameObserver observer) {

    }

    @Override
    public void setTileFocus(Position position) {

    }
}
