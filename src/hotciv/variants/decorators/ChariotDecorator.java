package hotciv.variants.decorators;


import hotciv.common.GameImpl;
import hotciv.common.UnitImpl;
import hotciv.framework.*;
import hotciv.variants.strategies.GammaCivUnitActionStrategy;

public class ChariotDecorator implements UnitActionStrategy{


    private GammaCivUnitActionStrategy actionStrategy;

    public ChariotDecorator(GammaCivUnitActionStrategy actionStrategy) {
        this.actionStrategy = actionStrategy;
    }

    @Override
    public void performUnitAction(Game g, Position p) {

        // If the unit is a chariot then perform action:

        GameImpl game = (GameImpl) g;
        UnitImpl unit = (UnitImpl) game.getUnitAt(p);

        if (unit.getTypeString().equals(GameConstants.CHARIOT)) {

            if (unit.isMoveable()) {
                unit.setDefensiveStrength(unit.getDefensiveStrength() * 2);
                unit.setMoveable(false);
            } else {
                unit.setDefensiveStrength(GameConstants.CHARIOT_DEFENSE_STRENGTH);
                unit.setMoveable(true);
            }
        }
        else {
            //Else perform same action as in GammaCivUnitActionStrategy
            actionStrategy.performUnitAction(g, p);
        }
    }
}
