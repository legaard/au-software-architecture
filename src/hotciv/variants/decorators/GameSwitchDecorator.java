package hotciv.variants.decorators;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

public class GameSwitchDecorator implements Game {

    Game currentGame;
    Game gameDecorator;

    public GameSwitchDecorator(Game game) {
        this.currentGame = game;
        this.gameDecorator = game;

    }

    public void changeState(){
        if(currentGame.equals(gameDecorator)){
            gameDecorator = currentGame;
            currentGame = new GameDecorator(currentGame);
        } else {
            currentGame = gameDecorator;
        }
    }

    public Tile getTileAt(Position p) throws IllegalPositionException {
        return currentGame.getTileAt(p);
    }

    public Unit getUnitAt(Position p) {
        return currentGame.getUnitAt(p);
    }

    public City getCityAt(Position p) {
        return currentGame.getCityAt(p);
    }

    public Player getPlayerInTurn() {
        return currentGame.getPlayerInTurn();
    }

    public Player getWinner() {
        return currentGame.getWinner();
    }

    public int getAge() {
        return currentGame.getAge();
    }

    public boolean moveUnit(Position from, Position to) {
        return currentGame.moveUnit(from, to);
    }

    public void endOfTurn() {
        currentGame.endOfTurn();
    }

    public void changeWorkForceFocusInCityAt(Position p, String balance) {
        currentGame.changeWorkForceFocusInCityAt(p, balance);
    }

    public void changeProductionInCityAt(Position p, String unitType) {
        currentGame.changeProductionInCityAt(p, unitType);
    }

    public void performUnitActionAt(Position p) {
        currentGame.performUnitActionAt(p);
    }

    @Override
    public void addObserver(GameObserver observer) {

    }

    @Override
    public void setTileFocus(Position position) {

    }
}
