package hotciv.common;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.*;

import java.security.KeyException;
import java.util.*;

public class GameImpl implements Game {

    private int turnCount;
    private int age;
    private int numberOfPlayers;

    private Player currentPlayer;
    private Map<Position,CityImpl> cityHashMap;
    private Map<Position, TileImpl> tileHashMap;
    private Map<Position, Unit> unitHashMap;
    private Map<Player, Integer> battleWinsHashMap;
    private ArrayList<GameObserver> observerArrayList;


    private WinnerStrategy winnerStrategy;
    private WorldAgingStrategy worldAgingStrategy;
    private UnitActionStrategy unitActionStrategy;
    private BattleStrategy battleStrategy;

    public GameImpl(GameFactory gameFactory) throws IllegalPositionException {

        this.winnerStrategy = gameFactory.createWinnerStrategy();
        this.worldAgingStrategy = gameFactory.createWorldAgingStrategy();
        this.unitActionStrategy = gameFactory.createUnitActionStrategy();
        this.battleStrategy = gameFactory.createBattleStrategy();

        turnCount = 0;
        age = -4000;
        currentPlayer = Player.RED;
        numberOfPlayers = 2;

        //Instantiate the different maps
        cityHashMap = new HashMap<Position, CityImpl>();
        tileHashMap = new HashMap<Position, TileImpl>();
        unitHashMap = new HashMap<Position, Unit>();
        battleWinsHashMap = new HashMap<Player, Integer>();
        observerArrayList = new ArrayList<GameObserver>();

        //Create world layout according to strategy
        gameFactory.createWorldLayoutStrategy().createWorld(this);
    }

    public Tile getTileAt(Position p) throws IllegalPositionException{

        if(tileHashMap.containsKey(p))  {
            return tileHashMap.get(p);
        } else return new TileImpl(GameConstants.PLAINS);
    }

    public Unit getUnitAt(Position p) {

        if(unitHashMap.containsKey(p)){
            return unitHashMap.get(p);
        } else return null;
    }

    public City getCityAt(Position p) {

        if(cityHashMap.containsKey(p)){
            return cityHashMap.get(p);
        } else return null;
    }

    public Player getPlayerInTurn() {

        return currentPlayer;
    }

    public Player getWinner() {

        return winnerStrategy.checkWinner(this);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int newAge){
        age = newAge;
    }

    public boolean moveUnit(Position from, Position to) {

        int rowDiff = Math.abs(from.getRow() - to.getRow());
        int columnDiff = Math.abs(from.getColumn() - to.getColumn());

        // If the unit doesn't exist on position: return false
        if(!unitHashMap.containsKey(from)){

            return false;
        }

        // If unit  doesn't have any moves left: return false
        if (unitHashMap.get(from).getMoveCount() < 1) {

            return false;
        }

        // If current player tries to move opponent's unit: return false
        if(unitHashMap.get(from).getOwner() != currentPlayer){

            return false;
        }

        // If the player tries to move more than row or column: return false
        if (rowDiff > 1 || columnDiff > 1) {

            return false;
        }

        if (!((UnitImpl) unitHashMap.get(from)).isMoveable()){

            return false;
        }

        // If unit moves across tiles of type mountain or ocean: return false
        if (tileHashMap.containsKey(to) &&
                (tileHashMap.get(to).getTypeString().equals(GameConstants.OCEANS) ||
                        tileHashMap.get(to).getTypeString().equals(GameConstants.MOUNTAINS))){

            return false;
        }

        // If unit moves to occupied tile (self-owned): return false
        if (unitHashMap.containsKey(to) && unitHashMap.get(to).getOwner() == currentPlayer) {

            return false;
        }

        // If player moves a unit to a tile occupied by the opponent: return true and initiate unitBattle
        if(unitHashMap.containsKey(to) && unitHashMap.get(to).getOwner() != currentPlayer) {

            if(battleStrategy.battle(this, from, to)){
                updateBattleWinsHashMap();
                removeUnitFromUnitHashMap(from, to);
                setWorldChangedAt(to);
                return true;
            } else {
                setWorldChangedAt(to);
                return false;
            }
        }

        // If the end destination is occupied by a city, then conquer.
        if(cityHashMap.containsKey(to) && cityHashMap.get(to).getOwner() != currentPlayer){

            conquerCity(cityHashMap.get(to), to, from);
            setWorldChangedAt(to);
            return true;
        }

        // If the above conditions are not met, then make a move
        else {

            conductMove(from, to);
            return true;
        }
    }

    public void conductMove(Position from, Position to){
        unitHashMap.put(to, unitHashMap.get(from));
        unitHashMap.remove(from);
        unitHashMap.get(to).move();
        setWorldChangedAt(to);
    }

    // Removes a unit from the HashMap, when there has been a successful battle.
    public void removeUnitFromUnitHashMap(Position from, Position to){
        removeUnitAt(to);
        try {
            addUnit(to, unitHashMap.get(from));
        } catch (KeyException e){
            e.printStackTrace();
        }
        removeUnitAt(from);
        getUnitAt(to).move();
    }

    public void updateBattleWinsHashMap(){
        if (battleWinsHashMap.containsKey(currentPlayer)){
            Integer currentWins = battleWinsHashMap.get(currentPlayer);
            battleWinsHashMap.put(currentPlayer, currentWins + 1);

        } else{
            battleWinsHashMap.put(currentPlayer, 1);
        }
    }

    public void endOfTurn() {

        turnCount++;

        if(currentPlayer == Player.BLUE) {
            currentPlayer = Player.RED;
        } else if (currentPlayer == Player.RED) {
            currentPlayer = Player.BLUE;
        }

        getWinner();

        if (turnCount % numberOfPlayers == 0) {
            endOfRound();
        }

        for (GameObserver observer : observerArrayList) {
            observer.turnEnds(currentPlayer, getAge());
        }

    }

    public int getRounds() {
        return turnCount / numberOfPlayers;
    }

    public void changeWorkForceFocusInCityAt( Position p, String balance ) {

    }

    public void changeProductionInCityAt( Position p, String unitType ) {

        if (cityHashMap.containsKey(p)) {
            cityHashMap.get(p).setProductionType(unitType);
        }
    }

    public void performUnitActionAt( Position p ) {

        if (getUnitAt(p).getOwner().equals(getPlayerInTurn())) {
            unitActionStrategy.performUnitAction(this, p);
        }
    }

    public void addObserver(GameObserver observer) {
        observerArrayList.add(observer);
    }

    public boolean removeObserver(GameObserver observer){

        return observerArrayList.remove(observer);
    }

    public void setTileFocus(Position position) {
        setTileFocusChangedAt(position);
    }

    private void endOfRound() {

        worldAgingStrategy.changeAge(this);
        resetMoveCounts();

        increaseProductionSizes();
        try {
            spawnUnit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetMoveCounts() {

        for (Unit unit : unitHashMap.values()) {
            unit.resetMoveCount();
        }
    }

    private void increaseProductionSizes() {

        for (CityImpl city: cityHashMap.values()) {
            city.increaseProductionSize();
        }
    }

    public void addUnit(Position p, Unit unit) throws KeyException {

        if(unitHashMap.containsKey(p)){
            throw new KeyException("Map already contains a unit at position (" + p.getRow() + ", " + p.getColumn() + ")");
        } else {
            unitHashMap.put(p, unit);
            setWorldChangedAt(p);
        }
    }

    public void addCity(Position p, CityImpl city) throws KeyException {

        if(cityHashMap.containsKey(p)){
            throw new KeyException("Map already contains a city at position (" + p.getRow() + ", " + p.getColumn() + ")");
        } else {
            cityHashMap.put(p, city);
            setWorldChangedAt(p);
        }
    }

    public void addTile(Position p, TileImpl tile) throws KeyException {
            tileHashMap.put(p, tile);
    }


    // Handles setup of the world using a string array
    public void setWorldLayout(String[] layout) {

        String line;

        for (int r = 0; r < GameConstants.WORLDSIZE; r++){
            line = layout[r];

            for (int c = 0; c < GameConstants.WORLDSIZE; c++){
                char tileChar = line.charAt(c);
                String type = "error";
                if(tileChar == '.') { type = GameConstants.OCEANS; }
                if(tileChar == 'o') { type = GameConstants.PLAINS; }
                if(tileChar == 'M') { type = GameConstants.MOUNTAINS; }
                if(tileChar == 'f') { type = GameConstants.FOREST; }
                if(tileChar == 'h') { type = GameConstants.HILLS; }

                try {
                    Position p = new Position(r,c);
                    addTile(p, new TileImpl(type));
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    /* Accessor */

    public Map<Position, CityImpl> getCityHashMap(){ return cityHashMap; }

    public Map<Player, Integer> getBattleWinsHashMap() { return battleWinsHashMap; }

    // Changes ownership on a given city
    public void conquerCity(CityImpl city, Position to, Position from){

        if (city.getOwner() != currentPlayer){
            city.setOwner(currentPlayer);
            conductMove(from, to);
        }
    }

    public void removeUnitAt(Position p) {
        unitHashMap.remove(p);
        setWorldChangedAt(p);
    }

    public void setTileFocusChangedAt(Position position){
        for (GameObserver observer : observerArrayList) {
            observer.tileFocusChangedAt(position);
        }
    }

    public void setWorldChangedAt(Position position){
        for (GameObserver observer : observerArrayList) {
            observer.worldChangedAt(position);
        }
    }

    public void spawnUnit() {

        for (Position position : cityHashMap.keySet()){

            CityImpl city = (CityImpl) getCityAt(position);
            ArrayList<Position> positionArrayList = getNearbyPositions(position);

            int productionSize = city.getProductionSize();
            int unitProductionCost = city.getProductionCost();

            if (productionSize >= unitProductionCost) {
                try {
                    if (!unitHashMap.containsKey(position)) {
                        city.decreaseProductionSize(unitProductionCost);
                        addUnit(position, new UnitImpl(city.getOwner(), city.getProduction()));
                    }
                    else {
                        city.decreaseProductionSize(unitProductionCost);

                        for (Position p : positionArrayList){
                            if (unitHashMap.containsKey(p)){
                                continue;
                            }
                            addUnit(p, new UnitImpl(city.getOwner(), city.getProduction()));
                            break;
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

    public ArrayList<Position> getNearbyPositions(Position position){

        ArrayList<Position> positionArrayList = new ArrayList<Position>();

        int rs = position.getRow();
        int cs = position.getColumn();

        int[] rowDifference = new int[]{rs - 1, rs - 1 , rs, rs + 1, rs + 1, rs + 1, rs, rs - 1};
        int[] columnDifference = new int[]{cs, cs + 1, cs + 1, cs + 1, cs, cs - 1, cs - 1, cs - 1};

        for (int i = 0; i < rowDifference.length; i++){

            try {
                String tileType = getTileAt(new Position(rowDifference[i], columnDifference[i])).getTypeString();

                if (i < GameConstants.WORLDSIZE &&
                        tileType != GameConstants.MOUNTAINS &&
                        tileType != GameConstants.OCEANS)
                    positionArrayList.add(new Position(rowDifference[i], columnDifference[i]));

            } catch (IllegalPositionException e){
                e.printStackTrace();
            }
        }

        return positionArrayList;

    }

    public void resetBattleWinsHashMap() {
        battleWinsHashMap.clear();
    }
}
