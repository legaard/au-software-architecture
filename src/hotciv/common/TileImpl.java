package hotciv.common;

import hotciv.exceptions.IllegalPositionException;
import hotciv.framework.Tile;

public class TileImpl implements Tile{

    String type;

    public TileImpl(String type) throws IllegalPositionException {

        this.type = type;
    }

    public String getTypeString() {
        return type;
    }
}
