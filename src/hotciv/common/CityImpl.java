package hotciv.common;

import hotciv.framework.City;
import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

public class CityImpl implements City {

    private Player owner;
    private int populationSize;
    private int productionSize;
    private String unitType;
    private int productionCost;

    public CityImpl(Player owner){

        this.owner = owner;
        this.populationSize = 1;
        this.productionSize = 0;
        setProductionType(GameConstants.ARCHER);
    }

    public Player getOwner() {
        return owner;
    }

    public int getSize() {
        return populationSize;
    }

    public String getProduction() {
        return unitType;
    }

    public void setProductionType(String unitType) {

        if(unitType.equals(GameConstants.ARCHER)) {
            this.unitType = unitType;
            setProductionCost(unitType);
        }

        if(unitType.equals(GameConstants.LEGION)) {
            this.unitType = unitType;
            setProductionCost(unitType);
        }

        if(unitType.equals(GameConstants.SETTLER)) {
            this.unitType = unitType;
            setProductionCost(unitType);
        }

        if(unitType.equals(GameConstants.CHARIOT)) {
            this.unitType = unitType;
            setProductionCost(unitType);
        }
    }

    public int getProductionCost(){
        return productionCost;
    }

    private void setProductionCost(String typeString){
        if (typeString.equals(GameConstants.ARCHER)){
            productionCost = GameConstants.ARCHER_COST;
        }
        if (typeString.equals(GameConstants.SETTLER)){
            productionCost = GameConstants.SETTLER_COST;
        }
        if (typeString.equals(GameConstants.LEGION)){
            productionCost = GameConstants.LEGION_COST;
        }
        if (typeString.equals(GameConstants.CHARIOT)){
            productionCost = GameConstants.CHARIOT_COST;
        }
    }

    public String getWorkforceFocus() { return null; }

    public int getProductionSize(){
        return productionSize;
    }

    public void increaseProductionSize(){
        productionSize += 6;
    }

    public void decreaseProductionSize(int decreaseBy){
        productionSize = productionSize - decreaseBy;
    }

    public void setOwner(Player player){
        owner = player;
    }
}
