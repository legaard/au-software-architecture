package hotciv.common;

import hotciv.framework.GameConstants;
import hotciv.framework.Player;
import hotciv.framework.Unit;

public class UnitImpl implements Unit {

    private Player owner;
    private String type;
    private int moveCount;
    private int defenseStrength;
    private int attackStrength;
    public boolean moveable;

    public UnitImpl(Player owner, String type){

        this.type = type;
        this.owner = owner;
        moveCount = 1;
        moveable = true;


        instantiateDefensiveStrength(type);
        instantiateAttackStrength(type);
    }

    public String getTypeString() {
        return type;
    }

    public Player getOwner() {
        return owner;
    }

    public int getMoveCount() {
        return moveCount;
    }

    public void move() {
        moveCount--;
    }

    public int getDefensiveStrength() {

        return defenseStrength;
    }

    public void instantiateDefensiveStrength(String type) {

        if (type.equals(GameConstants.ARCHER)) {
            defenseStrength = GameConstants.ARCHER_DEFENSE_STRENGTH;
        }

        if (type.equals(GameConstants.LEGION)) {
            defenseStrength = GameConstants.LEGION_DEFENSE_STRENGTH;
        }

        if (type.equals(GameConstants.SETTLER)) {
            defenseStrength = GameConstants.SETTLER_DEFENSE_STRENGTH;
        }

        if (type.equals(GameConstants.CHARIOT)) {
            defenseStrength = GameConstants.CHARIOT_DEFENSE_STRENGTH;
        }
    }

    public void instantiateAttackStrength(String type) {

        if (type.equals(GameConstants.ARCHER)) {
            attackStrength = GameConstants.ARCHER_ATTACK_STRENGTH;
        }

        if (type.equals(GameConstants.LEGION)) {
            attackStrength = GameConstants.LEGION_ATTACK_STRENGTH;
        }

        if (type.equals(GameConstants.SETTLER)) {
            attackStrength = GameConstants.SETTLER_ATTACK_STRENGTH;
        }

        if (type.equals(GameConstants.CHARIOT)) {
            attackStrength = GameConstants.CHARIOT_ATTACK_STRENGTH;
        }
    }

    public void setDefensiveStrength(int strength) {

        defenseStrength = strength;
    }

    public int getAttackingStrength() {

        return attackStrength;
    }

    public void resetMoveCount() {

        moveCount = 1;
    }

    public void setMoveable(boolean moveable) {

        this.moveable = moveable;
    }

    public boolean isMoveable() {

        return moveable;
    }

}
