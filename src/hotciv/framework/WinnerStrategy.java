package hotciv.framework;

public interface WinnerStrategy {

    public Player checkWinner(Game g);
}
