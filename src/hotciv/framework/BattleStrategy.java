package hotciv.framework;

public interface BattleStrategy {
    public boolean battle(Game game, Position from, Position to);
}
