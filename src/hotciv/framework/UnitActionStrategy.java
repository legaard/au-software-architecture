package hotciv.framework;

public interface UnitActionStrategy {

    public void performUnitAction(Game g, Position p);
}
