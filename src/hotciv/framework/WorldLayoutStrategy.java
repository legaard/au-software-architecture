package hotciv.framework;


public interface WorldLayoutStrategy {

    public void createWorld(Game game);
}
