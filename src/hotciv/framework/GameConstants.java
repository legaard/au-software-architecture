package hotciv.framework;

public class GameConstants {
  // The size of the world is set permanently to a 16x16 grid 
  public static final int WORLDSIZE = 16;

  // Valid unit types
  public static final String ARCHER    = "archer";
  public static final String LEGION    = "legion";
  public static final String SETTLER   = "settler";
  public static final String CHARIOT   = "chariot";

  // Unit defensive strength
  public static final int ARCHER_DEFENSE_STRENGTH = 3;
  public static final int LEGION_DEFENSE_STRENGTH = 2;
  public static final int SETTLER_DEFENSE_STRENGTH = 3;
  public static final int CHARIOT_DEFENSE_STRENGTH = 1;

  // Unit attack strength
  public static final int ARCHER_ATTACK_STRENGTH = 2;
  public static final int LEGION_ATTACK_STRENGTH = 4;
  public static final int SETTLER_ATTACK_STRENGTH = 0;
  public static final int CHARIOT_ATTACK_STRENGTH = 3;

  // Valid terrain types
  public static final String PLAINS    = "plains";
  public static final String OCEANS    = "ocean";
  public static final String FOREST    = "forest";
  public static final String HILLS     = "hills";
  public static final String MOUNTAINS = "mountain";

  // Unit costs
  public static final int ARCHER_COST = 10;
  public static final int LEGION_COST = 15;
  public static final int SETTLER_COST = 30;
  public static final int CHARIOT_COST = 20;

  // Valid production balance types
  public static final String productionFocus = "hammer";
  public static final String foodFocus = "apple";
}
