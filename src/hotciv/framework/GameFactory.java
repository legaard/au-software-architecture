package hotciv.framework;

public interface GameFactory {

    public WorldLayoutStrategy createWorldLayoutStrategy();

    public WorldAgingStrategy createWorldAgingStrategy();

    public UnitActionStrategy createUnitActionStrategy();

    public BattleStrategy createBattleStrategy();

    public WinnerStrategy createWinnerStrategy();
}
